<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@


	The docbook-to-man binary is found in the docbook-to-man package.
	Please remember that if you create the nroff version in one of the
	debian/rules file targets (such as build), you will need to include
	docbook-to-man in your Build-Depends control field.

  -->

  <!ENTITY dhfirstname "<firstname>Filippo</firstname>">
  <!ENTITY dhsurname   "<surname>Rusconi</surname>">
  <!ENTITY dhdate      "<date>December 3rd, 2017</date>">
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>lopippo@debian.org</email>">
  <!ENTITY dhusername  "Filippo Rusconi">
  <!ENTITY dhucpackage "<refentrytitle>COMET-MS</refentrytitle>">
  <!ENTITY dhpackage   "comet-ms">

  <!ENTITY softname   "comet-ms">
  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2014</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname> <refpurpose> The &dhpackage package
      ships the &dhpackage binary that does MS/MS database
      searches.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
    </cmdsynopsis>
    <para> Running &softname; with no arguments outputs a helping synopsis. </para>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the
      <application>&dhpackage;</application> source package.</para>

    <para> Comet is an open source tandem mass spectrometry (MS/MS)
      sequence database search engine. It identifies peptides by
      searching MS/MS spectra against sequences present in protein
      sequence databases.
</para>

    <para>This manual page was written for the &debian; distribution
      because the original program does not have a manual page.</para>

    <para>

      The <application>&dhpackage;</application> source package is
      used to create the <application>&dhpackage;</application> binary
      program.
    </para>

  </refsect1>
  
  <refsect1>
    <title>AUTHOR</title>

    <para>
      This manual page was written by &dhusername; <&dhemail;> for
      the &debian; system (and may be used by others).  Permission is
      granted to copy, distribute and/or modify this document under
      the terms of the &gnu; General Public License, Version 3 or any
      later version published by the Free Software Foundation.
    </para>
    <para>
      On Debian systems, the complete text of the GNU General Public
      License can be found in /usr/share/common-licenses/GPL-3.
    </para>

  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->
